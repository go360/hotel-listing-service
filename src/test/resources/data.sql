/*
SQLyog Ultimate v8.82 
MySQL - 8.0.16 : Database - trivago
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE IF NOT EXISTS `trivago` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin ;

USE `trivago`;

/*Table structure for table `accommodation` */

DROP TABLE IF EXISTS `accommodation`;

CREATE TABLE `accommodation` (
    `id` bigint(20) NOT NULL AUTO_INCREMENT,
    `hotelierId` bigint(20) NOT NULL,
    `name` varchar(128) COLLATE utf8_bin NOT NULL,
    `rating` int(11) NOT NULL,
    `category` varchar(128) COLLATE utf8_bin NOT NULL,
    `image` varchar(128) COLLATE utf8_bin NOT NULL,
    `reputation` int(11) NOT NULL,
    `price` int(11) NOT NULL,
    `availability` int(11) NOT NULL,
    `location` bigint(128) NOT NULL,
    PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `accommodation` */

/*Table structure for table `location` */

DROP TABLE IF EXISTS `location`;

CREATE TABLE `location` (
    `id` bigint(20) NOT NULL AUTO_INCREMENT,
    `accommodation` bigint(20) DEFAULT NULL,
    `city` varchar(50) COLLATE utf8_bin DEFAULT NULL,
    `state` varchar(50) COLLATE utf8_bin DEFAULT NULL,
    `country` varchar(50) COLLATE utf8_bin DEFAULT NULL,
    `zip_code` int(11) DEFAULT NULL,
    `address` varchar(500) COLLATE utf8_bin DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `FK_location` (`accommodation`),
    CONSTRAINT `FK_location` FOREIGN KEY (`accommodation`) REFERENCES `accommodation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `location` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
