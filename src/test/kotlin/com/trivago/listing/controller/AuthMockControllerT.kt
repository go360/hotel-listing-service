package com.trivago.listing.controller

import com.trivago.listing.security.JwtToken
import com.trivago.listing.security.Provider
import org.hamcrest.CoreMatchers.`is`
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class AuthMockControllerT {

    @Test
    fun `generate and validate a JWT token`() {
        val userId = "89888899"
        val provider1: Provider = Provider(userId)
        val genToken = JwtToken.generateToken(provider1)

        val provider = JwtToken.validateToken(genToken)
        Assert.assertThat(userId, `is`(provider.userId))
    }
}