package com.trivago.listing.request

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.trivago.listing.utils.Category

@JsonIgnoreProperties(ignoreUnknown = true)
class FilterRequest {
    lateinit var rating: Integer
    lateinit var city: String
    lateinit var reputationBadge: String
    lateinit var availability: Integer
    lateinit var category: Category
}