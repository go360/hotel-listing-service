package com.trivago.listing.request

import com.fasterxml.jackson.annotation.JsonProperty
import com.trivago.listing.utils.Category
import com.trivago.listing.utils.ValidURL
import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull


data class AccommodationRequest(
        @field: [NotNull NotEmpty]
        val name: String,
        @field: [NotNull Min(0, message = "Rating min 0") Max(5, message = "Rating max 5")]
        val rating: Integer,
        @field: [NotNull]
        val category: Category,
        @field: [NotNull]
        val location: LocationRequest,
        @field: [NotNull NotEmpty ValidURL("Invalid Image URL")]
        val image: String,
        @field: [NotNull Min(0) Max(1000)]
        val reputation: Integer,
        @field: [NotNull Min(0)]
        val price: Integer,
        @field: [NotNull Min(0)]
        val availability: Integer) {

}

class LocationRequest() {
    @field: [NotNull NotEmpty]
    lateinit var city: String

    @field: [NotNull NotEmpty]
    lateinit var state: String

    @field: [NotNull NotEmpty]
    lateinit var country: String

    @JsonProperty("zip_code")
    @field: [NotNull NotEmpty Min(1000) Max(9999)]
    lateinit var zipCode: Integer

    @field: [NotNull NotEmpty]
    lateinit var address: String
}

