package com.trivago.listing.utils

enum class Category {
    hotel, alternative, hostel, lodge, resort, guesthouse
}