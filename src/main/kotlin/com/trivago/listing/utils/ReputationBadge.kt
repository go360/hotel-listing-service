package com.trivago.listing.utils

enum class ReputationBadge {
    red, yellow, green
}