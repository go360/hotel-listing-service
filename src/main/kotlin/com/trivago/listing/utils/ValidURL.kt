package com.trivago.listing.utils

import java.lang.annotation.Documented
import java.net.URL
import javax.validation.Constraint
import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext
import kotlin.reflect.KClass


@Target(AnnotationTarget.FUNCTION, AnnotationTarget.FIELD, AnnotationTarget.VALUE_PARAMETER)
@Retention(AnnotationRetention.RUNTIME)
@Documented
@Constraint(validatedBy = [URLConstraintValidator::class])
annotation class ValidURL(val message: String = "Invalid URL")

class URLConstraintValidator : ConstraintValidator<ValidURL, String> {

    @Override
    override fun isValid(value: String, context: ConstraintValidatorContext?): Boolean {
        return try {
            URL(value)?.let { return true }
        } catch (e: Exception) {
            return false
        }
    }
}

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.FIELD, AnnotationTarget.VALUE_PARAMETER)
@Retention(AnnotationRetention.RUNTIME)
@Documented
@Constraint(validatedBy = [InRangeValidator::class])
annotation class InRange(val message: String = "Value is out of range", val groups: Array<KClass<*>> = [], val payload: Array<KClass<out Any>> = [], val min: Int = Int.MIN_VALUE, val max: Int = Int.MAX_VALUE)

class InRangeValidator : ConstraintValidator<InRange, Int> {

    private var min: Int = Int.MIN_VALUE
    private var max: Int = Int.MAX_VALUE

    @Override
    override fun initialize(constraintAnnotation: InRange) {
        this.min = constraintAnnotation.min
        this.max = constraintAnnotation.max
    }

    @Override
    override fun isValid(value: Int, context: ConstraintValidatorContext): Boolean {
        return value == null || (value in min..max);
    }
}
