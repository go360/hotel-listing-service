package com.trivago.listing.service

import com.trivago.listing.model.Accommodation
import com.trivago.listing.repository.AccommodationRepository
import com.trivago.listing.repository.LocationRepository
import com.trivago.listing.request.AccommodationRequest
import com.trivago.listing.request.FilterRequest
import com.trivago.listing.response.AccommodationResponse
import org.springframework.beans.BeanUtils
import org.springframework.stereotype.Service
import java.util.*
import java.util.stream.Collectors
import javax.transaction.Transactional

@Service
class AccommodationService(private val accommodationRepository: AccommodationRepository, private val locationRepository: LocationRepository) {

    fun listAccommodations(hotelierId: String): List<AccommodationResponse> {
        return accommodationRepository.findAllByHotelierId(hotelierId).stream().map { it ->
            AccommodationResponse(it)
        }.collect(Collectors.toList())
    }

    fun getAccommodation(id: String): AccommodationResponse {
        return AccommodationResponse(accommodationRepository.findById(id).get())
    }

    @Synchronized
    @Transactional
    fun save(hotelierId: String, accommodationRequest: AccommodationRequest) {
        val accommodation = Accommodation(hotelierId)
        BeanUtils.copyProperties(accommodationRequest, accommodation)
        BeanUtils.copyProperties(accommodationRequest.location, accommodation.location)
        accommodationRepository.save(accommodation)
    }

    @Synchronized
    @Transactional
    fun update(id: String, accommodationRequest: AccommodationRequest) {
        val accommodation = accommodationRepository.findById(id).get()
        BeanUtils.copyProperties(accommodationRequest, accommodation)
        BeanUtils.copyProperties(accommodationRequest.location, accommodation.location)
        accommodationRepository.save(accommodation)
    }

    @Synchronized
    @Transactional
    fun delete(id: String) {
        accommodationRepository.deleteById(id)
    }

    fun filter(filterRequest: FilterRequest): List<AccommodationResponse> {
        return Collections.emptyList()
    }
}