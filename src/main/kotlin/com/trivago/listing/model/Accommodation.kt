package com.trivago.listing.model

import com.trivago.listing.utils.Category
import com.trivago.listing.utils.ReputationBadge
import org.hibernate.annotations.GenericGenerator
import javax.persistence.*

@Entity
@Table(indexes = [Index(columnList = "name")])
class Accommodation() {

    constructor(hotelierId: String) : this(){
        this.hotelierId=hotelierId
        this.reputationBadge = ReputationBadge.red
    }

    @Id
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @GeneratedValue(generator = "uuid")
    var id: String? = null

    @Column(nullable = false)
    lateinit var hotelierId: String

    @Column(nullable = false)
    lateinit var name: String

    @Column(nullable = false)
    lateinit var rating: Integer

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    lateinit var category: Category

    @Column(nullable = false)
    lateinit var image: String

    @Column(nullable = false)
    lateinit var reputation: Integer

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    lateinit var reputationBadge: ReputationBadge

    @Column(nullable = false)
    lateinit var price: Integer

    @Column(nullable = false)
    lateinit var availability: Integer

    @OneToOne(cascade = [CascadeType.ALL], fetch = FetchType.EAGER)
    var location: Location = Location()


}