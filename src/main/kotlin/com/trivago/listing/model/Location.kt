package com.trivago.listing.model

import org.hibernate.annotations.GenericGenerator
import javax.persistence.*


@Entity
class Location() {

    @Id
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @GeneratedValue(generator = "uuid")
    var id: String? = null

    @Column(nullable = false)
    lateinit var city: String

    @Column(nullable = false)
    lateinit var state: String

    @Column(nullable = false)
    lateinit var country: String

    @Column(nullable = false)
    lateinit var zipCode: Integer

    @Column(nullable = false)
    lateinit var address: String
}