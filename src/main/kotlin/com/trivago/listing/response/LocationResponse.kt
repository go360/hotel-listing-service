package com.trivago.listing.response

import com.fasterxml.jackson.annotation.JsonProperty

class LocationResponse {
    lateinit var city: String
    lateinit var state: String
    lateinit var country: String
    @JsonProperty("zip_code")
    lateinit var zipCode: String
    lateinit var address: String
}