package com.trivago.listing.response

import com.trivago.listing.model.Accommodation
import com.trivago.listing.utils.Category
import org.springframework.beans.BeanUtils

class AccommodationResponse() {

    constructor(accommodation: Accommodation) : this() {
        BeanUtils.copyProperties(accommodation, this)
    }

    lateinit var name: String
    lateinit var rating: Integer
    lateinit var category: Category
    lateinit var image: String
    lateinit var reputation: Integer
    lateinit var reputationBadge: String
    lateinit var price: Integer
    lateinit var availability: Integer
    lateinit var location: LocationResponse

}