package com.trivago.listing.repository

import com.trivago.listing.model.Accommodation
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.PagingAndSortingRepository

interface AccommodationRepository : PagingAndSortingRepository<Accommodation, String> {

    @Query("FROM Accommodation where hotelierId = ?1")
    fun findAllByHotelierId(hotelierId: String): List<Accommodation>

}