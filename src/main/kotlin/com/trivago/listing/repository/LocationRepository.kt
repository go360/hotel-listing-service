package com.trivago.listing.repository

import com.trivago.listing.model.Location
import org.springframework.data.repository.PagingAndSortingRepository
import java.util.*

interface LocationRepository : PagingAndSortingRepository<Location, UUID> {

}