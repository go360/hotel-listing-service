package com.trivago.listing.controller

import com.trivago.listing.request.AccommodationRequest
import com.trivago.listing.service.AccommodationService
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@Validated
@RestController
@RequestMapping("/accommodations")
class AccommodationController(var service: AccommodationService) {

    @GetMapping("/")
    fun all(@RequestHeader("user_id") userId: String): ResponseEntity<Any> {
        return ResponseEntity.ok(service.listAccommodations(userId))
    }

    @GetMapping("/{accommodation_id}")
    fun get(@PathVariable("accommodation_id") accommodationId: String): ResponseEntity<Any> {
        return ResponseEntity.ok().build();
    }

    @PostMapping("/")
    fun create(@RequestHeader("user_id") userId: String, @RequestBody @Valid accommodation: AccommodationRequest): ResponseEntity<Any> {
        return ResponseEntity.ok(service.save(userId, accommodation))
    }

    @PutMapping("/{accommodationId}")
    fun update(@PathVariable accommodationId: String, @RequestBody accommodation: AccommodationRequest): ResponseEntity<Any> {
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{accommodation_id}")
    fun delete(@PathVariable("accommodation_id") accommodationId: String): ResponseEntity<Any> {
        return ResponseEntity.ok().build();
    }


}