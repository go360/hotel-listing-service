package com.trivago.listing.controller

import com.trivago.listing.security.JwtToken
import com.trivago.listing.security.Provider
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/booking")
class BookingController {

    @PostMapping("/accommodation")
    fun accommodation(@RequestParam accommodationId: String): ResponseEntity<Any> {
        return ResponseEntity(HttpStatus.NO_CONTENT)
    }
}