package com.trivago.listing.controller

import com.trivago.listing.security.JwtToken
import com.trivago.listing.security.Provider
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController


/**
 * A Mock class to handle the Authentication and Authorization
 * This token class will just return a token based on anyString (UserID)
 * I assumed that we have complete authentication and authorization module integrated
 * */

@RestController
@RequestMapping("/mocks")
class AuthMockController {

    @PostMapping("/auth")
    fun auth(@RequestParam("hotelierId") hotelierId: String): ResponseEntity<String> {
        var token = JwtToken.generateToken(Provider(hotelierId))
        val headers: MultiValueMap<String, String> = LinkedMultiValueMap()
        headers.add("X-Authorization", token)
        return ResponseEntity(headers,HttpStatus.NO_CONTENT)
    }

}