package com.trivago.listing.security

import com.fasterxml.jackson.databind.ObjectMapper
import io.jsonwebtoken.Claims
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import java.io.UnsupportedEncodingException
import java.util.*

object JwtToken {
    private const val secret = "some-secret-from-config"
    private const val expiration = 2592000000L
    private val mapper = ObjectMapper()

    @Throws(UnAuthorizedAccessException::class)
    fun validateToken(token: String): Provider {
        return try {
            val claims = getClaimsFromToken(token)
            if (claims == null)
                throw UnAuthorizedAccessException(TOKEN_ERROR)
            else
                mapper.convertValue(claims, Provider::class.java)
        } catch (e: Exception) {
            throw UnAuthorizedAccessException(TOKEN_ERROR)
        }
    }

    @Throws(UnsupportedEncodingException::class)
    fun generateToken(provider: Provider): String {
        val claims: MutableMap<String, Any> = HashMap()
        claims["sub"] = provider.userId
        claims["created"] = generateCurrentDate()
        return generateToken(claims)
    }

    private fun getExpirationDateFromToken(token: String): Date? {
        val expiration: Date?
        expiration = try {
            val claims = getClaimsFromToken(token)
            claims!!.expiration
        } catch (e: Exception) {
            null
        }
        return expiration
    }

    private fun getClaimsFromToken(token: String): Claims? {
        return Jwts.parser().setSigningKey(secret.toByteArray(charset("UTF-8")))
                .parseClaimsJws(token).body
    }

    private fun generateCurrentDate(): Date {
        return Date(System.currentTimeMillis())
    }

    private fun generateExpirationDate(): Date {
        return Date(System.currentTimeMillis() + expiration * 1000)
    }

    private fun isTokenExpired(token: String): Boolean {
        val expiration = getExpirationDateFromToken(token)
        return expiration!!.before(generateCurrentDate())
    }

    private fun generateToken(claims: Map<String, Any>): String {
        return generateToken("IAM", claims)
    }

    private fun generateToken(subject: String, claims: Map<String, Any>): String {
        return try {
            Jwts.builder().setClaims(claims).setSubject(subject).setExpiration(generateExpirationDate())
                    .signWith(SignatureAlgorithm.HS512, secret.toByteArray(charset("UTF-8"))).compact()
        } catch (ex: UnsupportedEncodingException) {
            // didn't want to have this method throw the exception, would rather log it and
            // sign the token like it was before
            Jwts.builder().setClaims(claims).setExpiration(generateExpirationDate())
                    .signWith(SignatureAlgorithm.HS512, secret).compact()
        }
    }
}