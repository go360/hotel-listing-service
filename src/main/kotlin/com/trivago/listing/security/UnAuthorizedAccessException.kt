package com.trivago.listing.security

const val TOKEN_ERROR = "INVALID TOKEN"

class UnAuthorizedAccessException(error: String) : RuntimeException(error) {

}