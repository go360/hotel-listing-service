package com.trivago.listing.security

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class Provider(var userId: String) {
   constructor():this("")
}